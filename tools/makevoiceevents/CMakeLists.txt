set(makevoiceevents_SRCS main.cpp)

include_directories(
    ${CMAKE_SOURCE_DIR}/src
    )

add_executable(lomiri-history-makevoiceevents ${makevoiceevents_SRCS})

target_link_libraries(lomiri-history-makevoiceevents Qt5::Core lomirihistoryservice)
install(TARGETS lomiri-history-makevoiceevents RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})
