# QML plugin

set(plugin_SRCS
    historyeventmodel.cpp
    historygroupedeventsmodel.cpp
    historygroupedthreadsmodel.cpp
    historymanager.cpp
    historymodel.cpp
    historyqmlfilter.cpp
    historyqmlintersectionfilter.cpp
    historyqmlplugin.cpp
    historyqmlsort.cpp
    historyqmltexteventattachment.cpp
    historyqmlunionfilter.cpp
    historythreadmodel.cpp
    )

set(plugin_HDRS
    historyeventmodel.h
    historygroupedeventsmodel.h
    historygroupedthreadsmodel.h
    historymanager.h
    historymodel.h
    historyqmlfilter.h
    historyqmlintersectionfilter.h
    historyqmlplugin.h
    historyqmlsort.h
    historyqmltexteventattachment.h
    historyqmlunionfilter.h
    historythreadmodel.h
)

include_directories(
    ${CMAKE_SOURCE_DIR}/src
    )

add_library(lomirihistory-qml MODULE ${plugin_SRCS} ${plugin_HDRS})

target_link_libraries(lomirihistory-qml
    Qt5::Contacts
    Qt5::Core
    Qt5::Qml
    Qt5::Quick
    lomirihistoryservice
    )

set(PLUGIN_DIR ${QT_INSTALL_QML}/Lomiri/History)
install(TARGETS lomirihistory-qml DESTINATION ${PLUGIN_DIR})
install(FILES qmldir DESTINATION ${PLUGIN_DIR})
